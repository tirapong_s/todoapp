import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import Todologin from './pages/index'
import TodoList from './pages/TodoList'
import logo from './logo.svg'
const NotFoundPage = () => <div>not</div>

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            active: false
        };
    }
    componentDidMount () {
        
    }

    render() {
        return (
            <Switch>
                <Route exact path="/" component={Todologin} />
                <Route exact path="/TodoList" component={TodoList} />
                <Route component={NotFoundPage} />
            </Switch>
        )
    }
}

export default App
