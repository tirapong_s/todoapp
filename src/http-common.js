import axios from "axios";
import Cookies from "js-cookie";
const token = Cookies.get(`Token`) ;
export default axios.create(
  {
    baseURL: "https://candidate.neversitup.com/todo/",
    headers: {
      "Content-type": "application/json",
      "Authorization": `Bearer ${token}`,
    },
  }
);
