import React, { useState, useEffect } from "react";
import { Button, Modal, Checkbox } from "antd";
import { DownloadOutlined } from "@ant-design/icons";
import todoService from "../services/todoService";

const TodoList = () => {
  const Todolist = {
    title: "",
    description: "",
  };
  const deletTodoList = {
    id: "",
  };
  const [todolist, setTodo] = useState(Todolist);
  const [statusmodal, Setmodul] = useState(false);
  const [datalist, settodolist] = useState([]);
  const [status, setstatus] = useState([]);
  const [statusDelete, setDelete] = useState(false);
  const [idTodo, setTododelete] = useState(deletTodoList);
  useEffect(() => {
    getTutorial();
  }, []);
  const getTutorial = () => {
    todoService
      .get("todos")
      .then((response) => {
        settodolist(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setTodo({ ...todolist, [name]: value });
  };
  const handleDeleteTodo = (obj) => {
    // const { name, value , id , user_id } = event.target.obj;
    // setTododelete({ ...idTodo, [user_id] : event.target.obj._id });
    const deleteurl = "todos/" + obj._id;
    todoService
      .remove(deleteurl)
      .then((response) => {
        window.location.reload();
        Setmodul(false);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const saveTutorial = () => {
    var data = {
      title: todolist.title,
      description: todolist.description,
    };
    if (status == "Add Todo") {
      todoService
        .create("todos", data)
        .then((response) => {
          setTodo(...todolist, {
            title: response.data.title,
            description: response.data.description,
          });
          Setmodul(false);
        })
        .catch((e) => {
          console.log(e);
        });
    } else {
      const updateurl = "todos/" + todolist.id;
      todoService
        .update(updateurl, data)
        .then((response) => {
          setTodo({
            title: response.data.title,
            description: response.data.description,
          });
          Setmodul(false);
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };
  const AddTodo = () => {
    setTodo(Todolist);
    Setmodul(true);
    setstatus("Add Todo");
  };
  const DeleteTodo = () => {
    setDelete(!statusDelete);
    if (statusDelete) {
    } else {
    }
  };
  const handleCancel = () => {
    Setmodul(false);
  };
  const setUpdate = (datalist, index) => {
    if (statusDelete) {
    } else {
      Setmodul(true);
      setstatus("Update Todo");
      setTodo({
        ...todolist,
        title: datalist.title,
        description: datalist.description,
        id: datalist._id,
      });
    }
  };
  return (
    <div className="limiter">
      <div className="container-login100">
        <div className="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-30 p-4">
          <div className="row" style={{ textAlign: "center" }}>
            <div className="col-12">
              {datalist &&
                datalist.map((datalist, index) => (
                  <div
                    className="card mt-3"
                    key={index}
                    onClick={() => setUpdate(datalist, index)}
                    style={{ cursor: "pointer" }}
                  >
                    <div className="card-body">
                      <div className="row">
                        <div className="col-8"> {datalist.title}</div>
                        <div className="col-4">
                          {" "}
                          {statusDelete ? (
                            <Button
                              type="danger"
                              shape="circle"
                              onClick={() => handleDeleteTodo(datalist)}
                            >
                              X
                            </Button>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
            <div className="col-md-12 mt-3" style={{ textAlign: "center" }}>
              <button
                className="btn btn-primary center-block"
                onClick={AddTodo}
              >
                {" "}
                + Create
              </button>
              <button
                className="btn btn-danger center-block ml-4"
                onClick={DeleteTodo}
              >
                {" "}
                - Delete
              </button>
            </div>
          </div>
        </div>
      </div>
      <Modal
        title={status}
        visible={statusmodal}
        onCancel={handleCancel}
        footer={null}
      >
        <div>
          <div className="form-group">
            <label htmlFor="title">Title</label>
            <input
              type="text"
              className="form-control"
              id="title"
              required
              value={todolist.title}
              onChange={handleInputChange}
              name="title"
            />
          </div>
          <div className="form-group">
            <label htmlFor="description">Description</label>
            <input
              type="text"
              className="form-control"
              id="description"
              required
              value={todolist.description}
              onChange={handleInputChange}
              name="description"
            />
          </div>
          <button onClick={saveTutorial} className="btn btn-success">
            Submit
          </button>
        </div>
      </Modal>
      {/* <Modal
        title="Delete Todo ? "
        visible={statusDelete}
        onCancel={handleCancel}
        footer={null}
      >
        <div>
        Do you want to delete this Todo?
        </div>
      </Modal> */}
    </div>
  );
};
export default TodoList;
