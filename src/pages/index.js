import React, { useState } from "react";
import { Form, Input, Button, Checkbox } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import todoService from "../services/todoService";
import Cookies from "js-cookie";

const Todologin = () => {
  const Logindefault = {
    username: "",
    password: "",
  };
  const [datalogin, setLogin] = useState(Logindefault);
  const [submitted, setSubmitted] = useState(false);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setLogin({ ...datalogin, [name]: value });
  };

  const Login = () => {
    var data = {
      username: datalogin.username,
      password: datalogin.password,
    };
    todoService
      .create("users/auth", data)
      .then((response) => {
        setLogin({
          token: response.data.token,
        });
        setSubmitted(true);
        Cookies.set("Token", response.data.token, { path: "/" });
        window.location = "/TodoList";
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <div class="limiter">
      <div class="container-login100">
        <div class="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-30 p-4">
          <div class="login100-form validate-form">
            <span class="login100-form-title p-b-55">Login</span>
            <div className="submit-form">
              <div>
                <div className="row mt-3">
                  <div className="col-12">
                    <label htmlFor="username">UserName</label>
                    <input
                      type="text"
                      className="form-control"
                      id="username"
                      required
                      value={datalogin.username}
                      onChange={handleInputChange}
                      name="username"
                    />
                  </div>
                  <div className="col-12 mt-2">
                    <label htmlFor="password">Password</label>
                    <input
                      type="password"
                      className="form-control"
                      id="password"
                      required
                      value={datalogin.password}
                      onChange={handleInputChange}
                      name="password"
                    />
                  </div>
                  <div className="col-12 mt-3" style={{ textAlign: "center" }}>
                    <button onClick={Login} className="btn btn-primary">
                      Login
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Todologin;
