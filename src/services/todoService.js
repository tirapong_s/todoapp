import http from "../http-common";

const get = (url,id )=> {
  return http.get(url);
};

const create = (url,data) => {
 return http.post(url, data);
};

const update = (url , data) => {
  return http.put(url, data);
};

const remove = (url) => {
  return http.delete(url);
};

export default {
  get,
  create,
  update,
  remove,
};
